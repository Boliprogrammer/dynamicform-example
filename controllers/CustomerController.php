<?php

namespace app\controllers;

use Yii;
use app\models\Customer;
use app\models\CustomerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Model;
use app\models\Address;
/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
/*    public function actionCreate()
    {
        $modelCustomer = new Customer();
        $modelsAddress = array();

        if ($modelCustomer->load(Yii::$app->request->post()) && $modelCustomer->save()) {
            return $this->redirect(['view', 'id' => $modelCustomer->id]);
        } else {
            return $this->render('create', [
                'modelCustomer' => $modelCustomer,
                'modelsAddress' => (empty($modelsAddress)) ? [new Address] : $modelsAddress
            ]);
        }
    }*/


    public function actionCreate()
    {

        $modelCustomer = new Customer;

        $modelsAddress = [new Address];


        if ($modelCustomer->load(Yii::$app->request->post())) {


            $modelsAddress = Model::createMultiple(Address::classname());

            Model::loadMultiple($modelsAddress, Yii::$app->request->post());


            // validate all models

            $valid = $modelCustomer->validate();

            $valid = Model::validateMultiple($modelsAddress) && $valid;


            if ($valid) {

                $transaction = \Yii::$app->db->beginTransaction();


                try {

                    if ($flag = $modelCustomer->save(false)) {

                        foreach ($modelsAddress as $modelAddress) {

                            $modelAddress->customer_id = $modelCustomer->id;

                            if (! ($flag = $modelAddress->save(false))) {

                                $transaction->rollBack();

                                break;

                            }

                        }

                    }


                    if ($flag) {

                        $transaction->commit();

                        return $this->redirect(['view', 'id' => $modelCustomer->id]);

                    }

                } catch (Exception $e) {

                    $transaction->rollBack();

                }

            }

        }    

                return $this->render('create', [

            'modelCustomer' => $modelCustomer,

            'modelsAddress' => (empty($modelsAddress)) ? [new Address] : $modelsAddress

        ]);

    }

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/


    /**

     * Updates an existing Customer model.

     * If update is successful, the browser will be redirected to the 'view' page.

     * @param integer $id

     * @return mixed

     */

    public function actionUpdate($id)

    {

        $modelCustomer = $this->findModel($id);

        // $modelsAddress = $modelCustomer->addresses;
        $modelsAddress = Address::find()->asArray()->all();


        if ($modelCustomer->load(Yii::$app->request->post())) {


            $oldIDs = ArrayHelper::map($modelsAddress, 'id', 'id');

            $modelsAddress = Model::createMultiple(Address::classname(), $modelsAddress);

            Model::loadMultiple($modelsAddress, Yii::$app->request->post());

            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsAddress, 'id', 'id')));


            // validate all models

            $valid = $modelCustomer->validate();

            $valid = Model::validateMultiple($modelsAddress) && $valid;


            if ($valid) {

                $transaction = \Yii::$app->db->beginTransaction();

                try {

                    if ($flag = $modelCustomer->save(false)) {

                        if (!empty($deletedIDs)) {

                            Address::deleteAll(['id' => $deletedIDs]);

                        }

                        foreach ($modelsAddress as $modelAddress) {

                            $modelAddress->customer_id = $modelCustomer->id;

                            if (! ($flag = $modelAddress->save(false))) {

                                $transaction->rollBack();

                                break;

                            }

                        }

                    }

                    if ($flag) {

                        $transaction->commit();

                        return $this->redirect(['view', 'id' => $modelCustomer->id]);

                    }

                } catch (Exception $e) {

                    $transaction->rollBack();

                }

            }

        }


        return $this->render('update', [

            'modelCustomer' => $modelCustomer,

            'modelsAddress' => (empty($modelsAddress)) ? [new Address] : $modelsAddress

        ]);

    }



    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
